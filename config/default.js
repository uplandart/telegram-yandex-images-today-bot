const NODE_ENV = process.env.NODE_ENV;
const BOT_TOKEN = process.env.BOT_TOKEN;
const DB_URL = process.env.DB_URL;

if (!BOT_TOKEN) throw new Error('Required to define BOT_TOKEN environment variable!');
// if (!DB_URL)    throw new Error('Required to define DB_URL environment variable!');

module.exports = {
  debug: NODE_ENV !== 'production',
  bot: {
    token: BOT_TOKEN,
  },
  db: {
    url: DB_URL,
  }
};