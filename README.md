# Telegram bot "Yandex.Images.Today"

name = Yandex.Images.Today  
username = YandexImagesTodayBot

Bot for receive daily image from yandex.ru

### list of commands
```
/get - get image with title
/get_title - get title
/get_image - get image
/get_link_author - get link of image author
/get_link_origin_image - get link to origin image
/set_receipt_time - set receipt time [DEF 07:00 AM +3] (soon)
/set_image_size - set image size [DEF origin] (soon)
/subscribe - subscribe for one time post image with title
/unsubscribe - unsubscribe from this event
/status - display user status
/keyboard - display keyboard buttons (soon)
/author - display author of the bot
/help - show help information
```

## Environment

### Back End
* [node.js](https://nodejs.org/en/) (>=v8.x.x)
* [mongodb](https://www.mongodb.com/) (>=v3.x.x)
* [pm2](http://pm2.keymetrics.io/) (>=v2.x.x)

## First Init

```
npm install
```
will install all modules (node_modules)

## Server start

```
KEY1=VALUE1 KEY2=VALUE2 ... pm2 start pm2.config.js
```
or
```
KEY1=VALUE1 KEY2=VALUE2 ... node index.js
```

`KEY1=VALUE1 KEY2=VALUE2 ...` - this is the node.js environment variables,  
that need to define for your configuration

### Node.js environment variables

* `BOT_TOKEN` - telegram bot token (required)
* `DB_URL`    - url to mongodb (required)
* `NODE_ENV`  - `production` or other (default: `development`)