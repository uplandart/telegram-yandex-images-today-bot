import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import path from 'path';
import config from 'config';

const tsFormat = () => (new Date()).toLocaleTimeString();

export default function getLogger(module) {

  const pathTo = 'server' + module.filename.split('/server')[1];

  return new winston.Logger({
    transports: [
      new winston.transports.Console({
        timestamp: tsFormat,
        colorize: true,
        level: config.debug ? 'debug' : 'error',
        label: pathTo
      }),
      new DailyRotateFile({
        filename: path.join(__dirname, '..', '..', 'logs', `.log`),
        timestamp: tsFormat,
        datePattern: 'MM-dd-yyyy',
        prepend: true,
        level: config.debug ? 'verbose' : 'info',
        label: pathTo
      })
    ]
  })
}