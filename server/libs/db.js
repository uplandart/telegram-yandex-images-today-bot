import mongoose from 'mongoose';
import bluebird from 'bluebird';
import config from 'config';
import getLogger from './logger';

const log = getLogger(module);

let db;

function createConnection() {
	const dbURI = `mongodb://${config.db.url}`;
	const dbOptions = {
		useMongoClient: true,
		autoReconnect: true,
		// keepAlive: 1,
		// socketTimeoutMS: 30000,
	};
	mongoose.Promise = bluebird;
	const db = mongoose.connection;

	// db.on('connecting', () => {
	// 	log.debug(`MongoDB connecting`);
	// });
	db.on('error', (error) => {
		log.error('Error on MongoDB connection: ' + error);
		mongoose.disconnect();
	});
	db.on('connected', () => {
		log.info(`MongoDB connected to URI: ${dbURI}`);
	});
	db.on('reconnected', () => {
		log.debug('MongoDB reconnected');
	});
	db.on('disconnected', () => {
		mongoose.connect(dbURI, dbOptions);
	});

	mongoose.connect(dbURI, dbOptions);

	return db;
}

export function handleOnExit() {
	if (!db) return;
	db.close(() => {
		log.debug('Force to close the MongoDB connection');
		process.exit(0);
	});
}

export function getClientInstance() {
	if (!db) {
		db = createConnection();
	}
	return db;
}