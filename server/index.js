import * as fs from 'fs';
import path from 'path';
import * as db from './libs/db';
import getLogger from './libs/logger';
import BotApp from './bot/index';
import schedule from './schedule/index';
import YandexImagesTodayReplySingleton from './classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const PATH_TO_LOGS_DIR = path.join(__dirname, '..', 'logs');

/**
 * create logs directory if not exists
 */
if (!fs.existsSync(PATH_TO_LOGS_DIR)) {
  fs.mkdirSync(PATH_TO_LOGS_DIR);
}

/**
 * run server
 */
main()
  .then(() => log.info('server started'))
  .catch(err => log.error(err));

async function main() {
  /**
   * init db connection
   */
  db.getClientInstance();

  /**
   * init today image data
   */
  const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();
  await instanceYandexImagesTodayReply.execRequest();
  log.debug('instanceYandexImagesTodayReply request done', instanceYandexImagesTodayReply.getTitle());

  /**
   * init telegram bot
   */
  BotApp.init();

  /**
   * init schedule
   */
  schedule(BotApp);
}