import axios from 'axios';
import cheerio from 'cheerio';

export default class YandexImagesReply {
  constructor() {
    this._jsonData = null;
    this._requestUrl = 'https://yandex.ru/images/';
    this._downloadResolutions = [
        "480x480","851x315","960x960",
      "1040x1536","1150x1734","1280x768",
      "1280x960","1280x1024","1920x1200",
      "1920x1080","1920x1920","2048x1536",
      "2048x2048","2208x2208","2448x2448",
      "2560x1440","2560x1600","2880x1800"
    ];
    this._requestHeaders = {
      'Accept': 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01',
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
      'Date': 'Sun, 15 Oct 2017 01:01:45 GMT',
      'X-Requested-With': 'XMLHttpRequest',
    };
    this._queryJsonAttrContainer = '.b-501px';
    this._attrJsonContainer = 'data-bem';
  }
  async execRequest() {
    const requestResponse = await axios.request({
      url: this._requestUrl,
      headers: this._requestHeaders
    });
    const $ = cheerio.load(requestResponse.data);

    const el = $(this._queryJsonAttrContainer);
    const attrDataBem = el.attr(this._attrJsonContainer);
    if (!attrDataBem) return;

    this._jsonData = JSON.parse(attrDataBem);
  }
  getDownloadResolutions() {
    return this._downloadResolutions;
  }
  getDownloadResolutionByIndex(index) {
    return this._downloadResolutions[index];
  }
  getJsonData() {
    return this._jsonData;
  }
}