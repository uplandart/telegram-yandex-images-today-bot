import YandexImagesReply from './YandexImagesReply';

export default class YandexImagesTodayReply extends YandexImagesReply {
  constructor() {
    super();
    this._today = null;
    this._mainKey = this._queryJsonAttrContainer.substr(1);
  }
  async execRequest() {
    await super.execRequest();
    this._today = this._jsonData[this._mainKey].today;
  }
  getTitle() {
    return `<b>Title:</b><em> ${this._today.snippet.title}</em>
<b>Author:</b> ${this._today.author.name}
<b>Description:</b> ${this._today.snippet.description}`;
  }
  getAuthorLink() {
    return this._today.author.link;
  }
  getPictureOriginLink() {
    return 'https:' + this._today.slide.appropriate.url;
  }
}