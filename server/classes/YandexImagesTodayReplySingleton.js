import YandexImagesTodayReply from './YandexImagesTodayReply';

class YandexImagesTodayReplySingleton {
  constructor() {}

  static getInstance() {
    if (!this._instance) {
      YandexImagesTodayReplySingleton._instance = new YandexImagesTodayReply();
    }
    return YandexImagesTodayReplySingleton._instance;
  }
}
YandexImagesTodayReplySingleton._instance = null;

export default YandexImagesTodayReplySingleton;