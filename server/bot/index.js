import Telegraf, { memorySession } from 'telegraf';
import config from 'config';
import getLogger from '../libs/logger';
import commands from './commands';
import handles from './handles';
import actions from './actions';
import hears from './hears';
import markups from './markups';

const log = getLogger(module);

const BotApp = new Telegraf(config.bot.token);
BotApp.use(memorySession());

/**
 * Register response time middleware
  */
BotApp.use(async (ctx, next) => {
  const start = new Date();

  await next();

  const ms = new Date() - start;
  log.debug('response time %sms', ms);
});

/**
 * Catch app errors
 */
BotApp.catch(err => {
  log.error(err);
});

BotApp.init = () => {

  /**
   * Register bot commands
   */
  commands(BotApp);

  /**
   * Register bot handles
   */
  handles(BotApp);

  /**
   * Register bot actions
   */
  actions(BotApp);

  /**
   * Register bot hears
   */
  hears(BotApp);

  /**
   * Init bot markups
   */
  markups();

  BotApp.startPolling();
  log.info('telegram bot start polling');
};

export default BotApp;