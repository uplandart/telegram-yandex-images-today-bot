import getBySize from './get-by-size';

export default function (BotApp) {

  BotApp.action(/get_by_size:(.+)/, getBySize);
}