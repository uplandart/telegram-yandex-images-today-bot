import getLogger from '../../libs/logger';
import YandexImagesTodayReplySingleton from '../../classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();

export async function replyPictureBySize(ctx, size) {
  let url = null;
  if (size === 'origin') {
    url = instanceYandexImagesTodayReply.getPictureOriginLink();
  } else {
    url = `https://yandex.ru/images/today?size=${size}`;
  }
  await ctx.replyWithPhoto({ url });
}

export default async (ctx) => {
  log.debug('action getBySize', ctx.from.id, ctx.match, ctx.session.prevGetCommand);

  const prevGetCommand = ctx.session.prevGetCommand;
  const sizeIndex = ctx.match[1];
  const size = instanceYandexImagesTodayReply.getDownloadResolutionByIndex(Number(sizeIndex)) || 'origin';

  switch (prevGetCommand) {
    case 'get_image':
      await replyPictureBySize(ctx, size);
      break;
    case 'get':
    default:
      await ctx.replyWithHTML(instanceYandexImagesTodayReply.getTitle());
      await replyPictureBySize(ctx, size);
  }
}