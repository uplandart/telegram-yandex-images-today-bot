import * as markupInlineKeyboardImageSizes from './inlineKeyboardImageSizes';

export default () => {

  markupInlineKeyboardImageSizes.getInstance();
}