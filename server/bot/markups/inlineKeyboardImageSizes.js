import { Markup, Extra } from 'telegraf';
import getLogger from '../../libs/logger';
import YandexImagesTodayReplySingleton from '../../classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();
const callbackDataKey = 'get_by_size';

let instance = null;

export function init() {
  let keyboardButtons = [];

  keyboardButtons = instanceYandexImagesTodayReply.getDownloadResolutions()
      .map((o,i) => Markup.callbackButton(o,`${callbackDataKey}:${i}`));
  keyboardButtons.push(Markup.callbackButton('Original',`${callbackDataKey}:origin`));
  log.debug('keyboardButtons', keyboardButtons);

  return Extra.HTML()
      .markup((m) => m.inlineKeyboard(keyboardButtons, { columns: 3 }));
}

export function getInstance() {
  if (!instance) {
    instance = init();
  }
  return instance;
}