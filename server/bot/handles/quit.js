import { Markup } from 'telegraf';
import getLogger from '../../libs/logger';
// import Users from '../../models/Users';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('on /quit');
  try {
    const chatId = ctx.from.id;
    // const user = await Users.findOne({ chatId });

    log.debug('ctx.from', ctx.from.id, ctx.message.from.id);

    ctx.leaveChat();
  } catch (err) {
    log.error(err);
  }
}