import getLogger from '../../libs/logger';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command start', ctx.from.id);

  const replyMarkdown = `
Welcome to *Yandex.Images.Today*

/subscribe - to subscribe to get new image every day
/help - to show more information
`;

  await ctx.replyWithMarkdown(replyMarkdown);
}