import getLogger from '../../libs/logger';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command author', ctx.from.id);

  await ctx.reply('uplandart@gmail.com');
}