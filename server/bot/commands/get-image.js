import getLogger from '../../libs/logger';
import * as markupInlineKeyboardImageSizes from '../markups/inlineKeyboardImageSizes';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command getImage', ctx.from.id);

  ctx.session.prevGetCommand = 'get_image';

  await ctx.replyWithHTML(
      'Choose image preferred size:',
      markupInlineKeyboardImageSizes.getInstance()
  );
}