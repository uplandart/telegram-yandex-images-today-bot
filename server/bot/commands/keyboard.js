import { Markup, Extra } from 'telegraf';
import getLogger from '../../libs/logger';
import Users from '../../models/Users';

const log = getLogger(module);

function getExtraKeyboardByUser(user) {
  let action = [];
  if (!user || !user.isSubscribed) {
    action.push('✔ subscribe');
  }
  if (user && user.isSubscribed) {
    action.push('✖ unsubscribe');
  }
  return Extra.markup(
      Markup
          .keyboard([
            [ 'get' ],
            [ 'get (title)', 'get (image)' ],
            [ 'get (link author)', 'get (link origin image)' ],
            action
          ])
          .oneTime()
          .resize()
  );
}

export default async (ctx) => {
  log.debug('command keyboard', ctx.from.id);

  const chatId = ctx.from.id;
  log.debug('chatId', chatId);

  const user = await Users.findOne({ chatId });
  log.debug('user', user);

  await ctx.reply('', getExtraKeyboardByUser(user));
}