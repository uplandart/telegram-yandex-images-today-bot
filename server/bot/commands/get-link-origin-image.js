import getLogger from '../../libs/logger';
import YandexImagesTodayReplySingleton from '../../classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();

export default async (ctx) => {
  log.debug('command getLinkOriginImage', ctx.from.id);

  await ctx.reply(instanceYandexImagesTodayReply.getPictureOriginLink());
}