import getLogger from '../../libs/logger';
import * as markupInlineKeyboardImageSizes from '../markups/inlineKeyboardImageSizes';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command get', ctx.from.id);

  ctx.session.prevGetCommand = 'get';

  await ctx.replyWithHTML(
      'Choose image preferred size:',
      markupInlineKeyboardImageSizes.getInstance()
  );
}