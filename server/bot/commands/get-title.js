import getLogger from '../../libs/logger';
import YandexImagesTodayReplySingleton from '../../classes/YandexImagesTodayReplySingleton';

const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command getTitle', ctx.from.id);

  await ctx.replyWithHTML(instanceYandexImagesTodayReply.getTitle());
}