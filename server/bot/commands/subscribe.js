import getLogger from '../../libs/logger';
import Users from '../../models/Users';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command subscribe', ctx.from.id);

  const chatId = ctx.from.id;
  let user = await Users.findOne({ chatId });
  log.debug('user', user);

  if (!user) {
    user = new Users({
      chatId,
      firstName: ctx.from.first_name,
      lastName: ctx.from.last_name,
      isSubscribed: true,
    });
    user = await user.save();
    console.log('result insert user', user);

    await ctx.replyWithHTML('<b>You are successfully subscribed!</b>');
  } else {

    if (user.isSubscribed) {

      await ctx.replyWithHTML('You are already subscribed');

    } else {
      user.isSubscribed = true;
      user.subscriptionDate = new Date();

      user = await user.save();
      console.log('result update user', user);

      await ctx.replyWithHTML('<b>You are successfully subscribed!</b>');
    }
  }
}