import getLogger from '../../libs/logger';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command help', ctx.from.id);

  const replyMarkdown = `
Bot for receive daily image from yandex.ru
If you subscribed, 
then you will be receive image with title every day at 07:00 AM (UTC+03:00).
 
/get - get image with title
/get\\_title - get title
/get\\_image - get image
/get\\_link\\_author - get link to author of image
/get\\_link\\_origin\\_image - get link to origin image

/subscribe - subscribe for one time post image with title
/unsubscribe - unsubscribe from one time post (only for subscribed users)

/author - display author of the bot
/help - display this help information
  `;

  await ctx.replyWithMarkdown(replyMarkdown);
}