import getLogger from '../../libs/logger';
import Users from '../../models/Users';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command unsubscribe', ctx.from.id);

  const chatId = ctx.from.id;
  let user = await Users.findOne({ chatId });
  log.debug('user', user);

  if (!user || !user.isSubscribed) {

    await ctx.replyWithHTML("You aren't subscribed");

  } else {
    user.isSubscribed = false;

    user = await user.save();
    console.log('result update user', user);

    await ctx.replyWithHTML('<b>You are unsubscribed!</b>');
  }
}