import moment from 'moment';
import getLogger from '../../libs/logger';
import Users from '../../models/Users';

const log = getLogger(module);

export default async (ctx) => {
  log.debug('command status', ctx.from.id);

  const chatId = ctx.from.id;
  let user = await Users.findOne({ chatId });
  log.debug('user', user);

  if (!user) {

    await ctx.replyWithHTML("You didn't be subscribed");

  } else {

    const replyMarkdown = `
You are subscribed since ${moment(user.subscriptionDate || user.createdAt).format('DD MMM YYYY HH:mm')}
`;

    await ctx.replyWithHTML(replyMarkdown);
  }
}