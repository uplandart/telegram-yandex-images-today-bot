import start from './start';
import get from './get';
import getTitle from './get-title';
import getImage from './get-image';
import getLinkAuthor from './get-link-author';
import getLinkOriginImage from './get-link-origin-image';
import subscribe from './subscribe';
import unsubscribe from './unsubscribe';
import status from './status';
import keyboard from './keyboard';
import author from './author';
import help from './help';

export default function (BotApp) {

  /**
   * initial when user join to bot chat
   */
  BotApp.command('start', start);

  /**
   * pass to recipient parts of today image
   */
  BotApp.command('get', get);
  BotApp.command('get_title', getTitle);
  BotApp.command('get_image', getImage);
  BotApp.command('get_link_author', getLinkAuthor);
  BotApp.command('get_link_origin_image', getLinkOriginImage);

  /**
   * set only for subscribed users
   */
  // BotApp.command('set_receipt_time', get);
  // BotApp.command('set_image_size', get);

  /**
   * subscribe to receive image
   */
  BotApp.command('subscribe', subscribe);
  /**
   * unsubscribe from event
   */
  BotApp.command('unsubscribe', unsubscribe);

  /**
   * display user available information
   */
  BotApp.command('status', status);

  /**
   * set keyboard
   */
  // BotApp.command('keyboard', keyboard);

  /**
   * display author information
   */
  BotApp.command('author', author);

  /**
   * display more bot available information
   */
  BotApp.command('help', help);
}