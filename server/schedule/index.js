import schedule from 'node-schedule';
import getLogger from '../libs/logger';
import scheduleUpdateImage from './update-image';
import scheduleUpdateAndSendImage from './update-and-send-image';

const log = getLogger(module);

export default (BotApp) => {
  log.debug('schedule init');

  /**
   * every day at midnight
   */
  schedule.scheduleJob('0 0 * * *', scheduleUpdateImage(BotApp));

  /**
   * every day at 07:00 AM
   */
  schedule.scheduleJob('0 7 * * *', scheduleUpdateAndSendImage(BotApp));
}