import getLogger from '../libs/logger';
import YandexImagesTodayReplySingleton from '../classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();

export default (BotApp) => {
  return async () => {
    try {
      log.debug('updateImage', new Date());

      await instanceYandexImagesTodayReply.execRequest();
    } catch (err) {
      log.error(err);
    }
  }
}