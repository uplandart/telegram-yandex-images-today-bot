import getLogger from '../libs/logger';
import Users from '../models/Users';
import YandexImagesTodayReplySingleton from '../classes/YandexImagesTodayReplySingleton';

const log = getLogger(module);
const instanceYandexImagesTodayReply = YandexImagesTodayReplySingleton.getInstance();

export default (BotApp) => {
  return async () => {
    try {
      log.debug('updateAndSendImage', new Date());

      await instanceYandexImagesTodayReply.execRequest();

      const listSubscribedUsers = await Users.find({ isSubscribed: true });
      log.debug('listSubscribedUsers', listSubscribedUsers.length);

      for (let i = 0; i < listSubscribedUsers.length; i++) {
        const user = listSubscribedUsers[i];
        log.debug('user', user._id, user.firstName);

        try {
	        await BotApp.telegram.sendMessage(user.chatId, instanceYandexImagesTodayReply.getTitle(), {parse_mode: 'HTML'});
	        await BotApp.telegram.sendPhoto(user.chatId, {
		        url: instanceYandexImagesTodayReply.getPictureOriginLink()
	        });
        } catch (e) {
          log.error(e);
        }
      }
    } catch (err) {
      log.error(err);
    }
  }
}