import mongoose from 'mongoose';

const modelSchema = new mongoose.Schema({
  chatId: {
    type: String,
    required: true,
    unique: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  subscriptionDate: {
    type: Date,
    default: new Date(),
  },
  isSubscribed: {
    type: Boolean,
    default: false,
  }
}, {
  timestamps: true,
});

const Users = mongoose.model('users', modelSchema);

export default Users;