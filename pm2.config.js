const OS = require("os");

const cpuLength = OS.cpus().length;

let instances = 1;

// if (cpuLength > 1) {
// 	instances = cpuLength - 1;
// }

module.exports = {
	apps : [{
		name            : "CitisDevBot",
		script          : "./index.js",
		watch           : ["server","public","private"],
		ignore_watch    : ["node_modules","src"],

		instances       : instances,
		exec_mode       : "cluster",

		env             : {
			"NODE_ENV": "development",
		},
		env_production  : {
			"NODE_ENV": "production"
		},

		merge_logs      : true,
		log_date_format : "YYYY-MM-DD HH:mm:ss Z"
	}]
};