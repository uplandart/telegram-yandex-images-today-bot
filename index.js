#!/usr/bin/env node
'use strict';

let nodeVersion = process.versions.node;
let majorNodeVersion = Number(nodeVersion[0]);
if (majorNodeVersion < 8) {
  throw new Error('The version of Node.js must be 8.x.x or greater');
}

require('babel-register')({
  plugins: [
    'babel-plugin-transform-es2015-modules-commonjs'
  ]
});
require('./server/index');